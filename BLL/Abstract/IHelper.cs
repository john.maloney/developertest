﻿using BLL.Models;
using System;
using System.Collections.Generic;

namespace BLL.Abstract
{
    public interface IHelper
    {
        int? GetNextNumberOrNull(int number, List<int> integerList);
        bool DatesAreValid(List<DateTime> datesToValidate);
        Client GetClientById(List<Client> clients, int Id);
        List<string> GetFirstSetOfNames(List<string> names, int number);
        string ApplyStringInterpolation(string name);
        Tuple<DateTime, DateTime> GetDates(int days);
        string[] UniqueNames(string[] names1, string[] names2);
    }
}
