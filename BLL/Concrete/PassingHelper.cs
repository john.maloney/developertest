﻿using BLL.Abstract;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
    public class PassingHelper : IHelper
    {
        /// <summary>
        /// For a given int passed in, return the next highest int in the array.
        /// return null if not found
        public int? GetNextNumberOrNull(int number, List<int> integerList)
        {
            integerList.Sort();
            for (int i = 0; i < integerList.Count; i++)
            {
                if (integerList[i] > number)
                    return integerList[i];
            }

            return null;
        }


        /// <summary>
        /// Business Rules:  Valid dates meet the following criteria:  
        ///     - Falls on a Monday
        ///     - 1st of the month
        public bool DatesAreValid(List<DateTime> datesToValidate)
        {
            bool isInvalid = datesToValidate
                .Any(x => x.DayOfWeek != DayOfWeek.Monday && x.Day != 1);  

            //Any is more efficient - stops looking as soon as it finds the first true

            return !isInvalid;
        }


        public Client GetClientById(List<Client> clients, int Id)
        {
            var result = clients.Where(x => x.Id == Id).FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Return the first x number of names indicated by the parameter "number" from the list "names"
        /// </summary>
        /// <param name="names"></param>
        /// <param name="number"></param>
        /// <returns>A list of names</returns>
        public List<string> GetFirstSetOfNames(List<string> names, int number)
        {
            return names.Take(number).ToList();
        }

        /// <summary>
        /// Use string interpolation to achieve the same result as the expected result from unit test
        /// </summary>
        /// <param name="name"></param>
        /// <returns>string created using string interpolation</returns>
        public string ApplyStringInterpolation(string name)
        {
            return $"My name is {name}";
        }

        /// <summary>
        /// Start Date = Beginning of previous x number of days
        /// End Date = End of today
        /// </summary>
        /// <param name="days"></param>
        /// <returns>Start Date and End Date</returns>
        public Tuple<DateTime, DateTime> GetDates(int days)
        {
            var startDate = DateTime.Now.AddDays(days).Date;
            var endDate = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            return Tuple.Create(startDate, endDate);
        }

        public string[] UniqueNames(string[] names1, string[] names2)
        {
            var list = new List<string>();

            foreach (var name in names1)
            {
                list.Add(name);
            }

            foreach (var name in names2)
            {
                list.Add(name);
            }

            return list.Distinct().ToArray();
        }
    }
}
