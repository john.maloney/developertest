﻿using BLL.Abstract;
using System.Collections.Generic;

namespace BLL.Models
{
    public interface IForecastByTypeHandlerFactory
    {
        IForecastByTypeHandler Gethandler(ForecastByType forecastByType);
    }

    internal class ForecastByTypeHandlerFactory : IForecastByTypeHandlerFactory
    {
        #region Fields/Ctor

        private readonly IDictionary<ForecastByType, IForecastByTypeHandler> _forecastByTypeHandlerDictionary;

        public ForecastByTypeHandlerFactory(IDictionary<ForecastByType, IForecastByTypeHandler> forecastByTypeHandlerDictionary)
        {
            _forecastByTypeHandlerDictionary = forecastByTypeHandlerDictionary;
        }

        #endregion 

        #region Interface Methods

        public IForecastByTypeHandler Gethandler(ForecastByType forecastByType) => _forecastByTypeHandlerDictionary[forecastByType];

        #endregion 
    }
}
