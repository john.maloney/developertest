﻿namespace BLL.Models
{

    public enum ForecastByType
    {
        Supplier = 1,
        Account = 2,
        Customer = 3,
        CustomerLocation = 4,
        Item = 5 
    }
}