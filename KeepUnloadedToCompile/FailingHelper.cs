﻿using BLL.Abstract;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
    public class FailingHelper : IHelper
    {
        /// <summary>
        /// For a given int passed in, return the next highest int in the array.
        /// return null if not found
        public int? GetNextNumberOrNull(int number, List<int> integerList)
        {
            var nextNumber = integerList.Where(x => x == number);
            return nextNumber;
        }


        /// <summary>
        /// Business Rules:  Valid dates meet the following criteria:  
        ///     - Falls on a Monday
        ///     - 1st of the month
        public bool DatesAreValid(List<DateTime> datesToValidate)
        {
            var dateCount = datesToValidate.Count;
            int validDates = 0;

            foreach (var date in datesToValidate)
            {
                var isValid = true;

                if (date.Month != 1)
                    isValid = false;

                if (date.DayOfWeek == DayOfWeek.Monday)
                    isValid = false;

                validDates++;
            }

            if (validDates == dateCount)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public Client GetClientById(List<Client> clients, int Id)
        {
            return clients.Where(x => x.Id == 3).FirstOrDefault();
        }

        /// <summary>
        /// Return the number of names indicated by the parameter "number" from the list "names"
        /// </summary>
        public List<string> GetFirstSetOfNames(List<string> names, int number)
        {
            return new List<string>();
        }

        /// <summary>
        /// Use string interpolation to achieve the same result as the expected result from unit test
        /// </summary>
        public string ApplyStringInterpolation(string name)
        {
            return "";
        }

        /// <summary>
        /// Start Date = Beginning of previous x number of days
        /// End Date = End of today
        /// </summary>
        /// <param name="days"></param>
        /// <returns>Start Date and End Date</returns>
        public Tuple<DateTime, DateTime> GetDates(int days)
        {
            return null;
        }
    }
}
