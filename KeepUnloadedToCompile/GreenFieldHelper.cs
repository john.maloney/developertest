﻿using System;
using System.Collections.Generic;

namespace BLL
{
    public class GreenFieldHelper : IHelper
    {
        /// <summary>
        /// For a given int passed in, return the next highest int in the array.
        /// return null if not found
        public int? GetNextNumberOrNull(int number, List<int> integerList)
        {

            return null;
        }

        /// <summary>
        /// Business Rules:  Valid dates meet the following criteria:  
        ///     - Falls on a Monday
        ///     - 1st of the month
        public bool DatesAreValid(List<DateTime> datesToValidate)
        {

            return false;
        }

    }
}
