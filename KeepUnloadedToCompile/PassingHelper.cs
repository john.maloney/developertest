﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
    public class PassingHelper : IHelper
    {
        /// <summary>
        /// For a given int passed in, return the next highest int in the array.
        /// return null if not found
        public int? GetNextNumberOrNull(int number, List<int> integerList)
        {
            integerList.Sort();
            for (int i = 0; i < integerList.Count; i++)
            {
                if (integerList[i] > number)
                    return integerList[i];
            }

            return null;
        }


        /// <summary>
        /// Business Rules:  Valid dates meet the following criteria:  
        ///     - Falls on a Monday
        ///     - 1st of the month
        public bool DatesAreValid(List<DateTime> datesToValidate)
        {
            return false;
        }


        public Client GetClientById(List<Client> clients, int Id)
        {
            var result = clients.Where(x => x.Id == Id).FirstOrDefault();
            return result;
        }
    }
}
