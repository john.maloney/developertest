# DeveloperTest

Administrator Notes -

    There are 3 Concrete types that implement IHelper:

    1. PassingHelper - tests pass - use to confirm our unit tests are appropriate.

    2. FailingHelper - tests are failing and code doesn't compile - use to administor a "Refactoring scenario" or Code review scenario when the candidate doesn't have the means to pull, compile, and code.

    3. GreenfieldHelper - tests fail.  Candidate will have to implement Interface methods.

    The concrete helpers that arent't being used should be preserved in the "UnloadToCompile" project, so that the project can compile while we develop & run our unit tests. 

    Move the one you need to use into the BLL > Concrete folder and update the concrete helper type in the test project's constructor as needed.

