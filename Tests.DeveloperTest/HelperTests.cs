using System;
using System.Collections.Generic;
using Xunit;
using BLL;
using BLL.Models;
using BLL.Abstract;

namespace Tests.DeveloperTest
{
    public class HelperTests
    {
        /// <summary>
        ///             NOTE!! - change the concrete type in the ctor
        /// </summary>
        private IHelper _helper;

        public HelperTests()
        {
            _helper = new PassingHelper();  //  <--  Change to the Concrete IHelper you need.
        }

        [Fact]
        public void GetNextNumberOrNull_Returns_NextHighestNumber()
        {
            // Arrange
            var intList = new List<int> { 9, 1, 4, 7, 5 };

            // Act
            var nextHighestNumberInTheList = _helper.GetNextNumberOrNull(1, intList);
            var nextHighestNumberInTheList2 = _helper.GetNextNumberOrNull(6, intList);
            var nextHighestNumberInTheList3 = _helper.GetNextNumberOrNull(9, intList);

            //Assert
            Assert.Equal(4, nextHighestNumberInTheList);
            Assert.Equal(7, nextHighestNumberInTheList2);
            Assert.Null(nextHighestNumberInTheList3);
        }

        [Fact]
        public void Validate_Forecast_Date_List()
        {
            // Arrange
            List<DateTime> validDateList = new List<DateTime>()
            {
                new DateTime(2021, 4, 1),
                new DateTime(2021, 5, 1),
                new DateTime(2021, 6, 1),
                new DateTime(2021, 4, 5),
                new DateTime(2021, 4, 12),
                new DateTime(2021, 4, 19),
                new DateTime(2021, 4, 26)
            };
            List<DateTime> invalidDateList = new List<DateTime>()
            {
                new DateTime(2021, 4, 1),
                new DateTime(2021, 5, 1),
                new DateTime(2021, 6, 1),
                new DateTime(2021, 4, 5),
                new DateTime(2021, 4, 12),
                new DateTime(2021, 4, 19),
                new DateTime(2021, 4, 26),
                new DateTime(2021, 4, 7),
                new DateTime(2021, 4, 10),
                new DateTime(2021, 4, 15)
            };

            // Act
            var invalidDateList_isValid = _helper.DatesAreValid(invalidDateList);
            var validDateList_isValid = _helper.DatesAreValid(validDateList);

            // Assert
            Assert.False(invalidDateList_isValid);
            Assert.True(validDateList_isValid);
        }

        [Fact]
        public void Get_Client_By_Id()
        {
            // Arrange
            var clientList = new List<Client>
            {
                new Client { Id = 1, Name = "ABC Manufacturing"},
                new Client { Id = 2, Name = "EZ Shopping Network"},
                new Client { Id = 3, Name = "Reliable Widget Co."},
                new Client { Id = 4, Name = "Avacado Inc."},
            };

            // Act
            var clientFound = _helper.GetClientById(clientList, 3);
            var clientNotFound = _helper.GetClientById(clientList, 5);

            // Assert
            Assert.Equal("Reliable Widget Co.", clientFound.Name);
            Assert.Null(clientNotFound);
        }

        [Fact]
        public void Get_First_Set_Of_Names()
        {
            //Arrange
            var number = 5;
            List<string> nameList = new List<string>()
        {
            "David",
            "Henry",
            "Jeff",
            "Larry",
            "Jim",
            "Susan",
            "Laura",
            "Lucy",
            "Katy",
            "Betty"
        };

            //Act
            var names = _helper.GetFirstSetOfNames(nameList, number);

            //Assert
            Assert.Equal(number, names.Count);
        }

        [Fact]
        public void Apply_String_Interpolation()
        {
            //Arrange
            var name = "Terry";
            var expected = "My name is " + name;

            //Act
            var actual = _helper.ApplyStringInterpolation(name);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Get_Dates()
        {
            //Arrange
            var previousDays = -5;
            var expectedStartDate = Convert.ToDateTime(DateTime.Now.AddDays(previousDays).ToString("yyyy-MM-ddT00:00:00"));
            var expectedEndDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-ddT23:59:59"));

            //Act
            var dates = _helper.GetDates(previousDays);

            //Assert
            Assert.Equal(expectedStartDate, dates.Item1);
            Assert.Equal(expectedEndDate, dates.Item2);
        }

        [Fact]
        public void Get_Unique_Names()
        {
            //Arrange
            string[] names1 = new string[] { "Ava", "Emma", "Olivia" };
            string[] names2 = new string[] { "Olivia", "Sophia", "Emma" };
            var uniqueList = new List<string>()
            {
                 "Ava", "Emma", "Olivia", "Sophia"
            };

            //Act
            var uniqueNames = _helper.UniqueNames(names1, names2);

            //Assert
            Assert.Equal(4, uniqueNames.Length);
            Assert.True(uniqueList.Contains(uniqueNames[0]));
            Assert.True(uniqueList.Contains(uniqueNames[1]));
            Assert.True(uniqueList.Contains(uniqueNames[2]));
            Assert.True(uniqueList.Contains(uniqueNames[3]));
        }
    }
}
